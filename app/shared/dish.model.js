"use strict";
var Dish = (function () {
    function Dish(id, name, info, price) {
        this.id = id;
        this.name = name;
        this.info = info;
        this.price = price;
    }
    return Dish;
}());
exports.Dish = Dish;
//# sourceMappingURL=dish.model.js.map