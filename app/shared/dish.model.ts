export interface IDish{
    id: number;
    name:string;
    info:string;
    price: number;
}

export class Dish implements IDish{
    id: number;
    name:string;
    info:string;
    price: number;
    constructor(id: number,name:string,info:string,price: number){
        this.id = id;
        this.name = name;
        this.info = info;
        this.price = price
    }
}