import { Component,Input } from '@angular/core';

import { dishs } from '../shared/dish.data'
import { IDish } from '../shared/dish.model'

@Component({
    selector: 'menu',
    templateUrl : 'app/menu/menu.component.html',
    styleUrls: ['app/menu/menu.component.css'],
    directives: [],
    providers: []
})  

export class Menu{
    @Input() typeOfDish:string;

    dishs:IDish[];
    
    constructor(){
        
        this.dishs = dishs
    }

    changeTypeOfDish(typeOfDish:string){
        
        this.typeOfDish = typeOfDish;
    }
}