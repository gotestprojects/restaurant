import { Component } from '@angular/core';

import { Menu } from './menu/menu.component'
import { dishs } from './shared/dish.data'
import { IDish,Dish } from './shared/dish.model'

@Component({
    selector: 'menu-app',
    templateUrl : 'app/app.component.html',
    styleUrls: ['app/app.component.css'],
    directives: [Menu],
    providers: [Menu]
})  

export class AppComponent{
    typeOfDish:string;
    changeTypeOfDish:any;

    constructor(menu:Menu){
        
        let testdish = new Dish(1,"Борщ","Вах какой борщ", 100)
        this.typeOfDish = "";
        this.changeTypeOfDish = menu.changeTypeOfDish
        
        dishs.push(testdish)
    }

  
}