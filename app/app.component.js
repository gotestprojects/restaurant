"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var menu_component_1 = require('./menu/menu.component');
var dish_data_1 = require('./shared/dish.data');
var dish_model_1 = require('./shared/dish.model');
var AppComponent = (function () {
    function AppComponent(menu) {
        var testdish = new dish_model_1.Dish(1, "Борщ", "Вах какой борщ", 100);
        this.typeOfDish = "";
        this.changeTypeOfDish = menu.changeTypeOfDish;
        dish_data_1.dishs.push(testdish);
    }
    AppComponent = __decorate([
        core_1.Component({
            selector: 'menu-app',
            templateUrl: 'app/app.component.html',
            styleUrls: ['app/app.component.css'],
            directives: [menu_component_1.Menu],
            providers: [menu_component_1.Menu]
        }), 
        __metadata('design:paramtypes', [menu_component_1.Menu])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map