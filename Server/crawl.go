package main

import (
	"fmt"
	"net/http"

	"github.com/PuerkitoBio/goquery"

	"golang.org/x/net/html"
)

var (
	seedUrl   = "http://defcon.su/"
	foundUrls = make(map[string]bool)
	Articles  []Article
)

type Article struct {
	Title string `json:"title"`
	Body  string `json:"body"`
	Time  string `json:"time"`
	Tags  string `json:"tags"`
	Link  string `json:"link"`
}

// Helper function to pull the href attribute from a Token
func getHref(t html.Token) (href string) {
	for _, a := range t.Attr {
		if a.Key == "href" {

			if len(a.Val) > len(seedUrl) && a.Val[:len(seedUrl)] == seedUrl {
				href = a.Val
			}
		}
	}
	return
}
func crawlArticle(url string) Article {
	Art := Article{}

	Art.Link = url
	doc, err := goquery.NewDocument(url)
	if err != nil {
		fmt.Println(err)
	}

	doc.Find("article").Each(func(i int, s *goquery.Selection) {
		Art.Title = s.Find(".art-header-inner h2").Text()
		Art.Time = s.Find(".art-header-inner time").Text()
		Art.Tags, _ = s.Find(".art-header-inner p").Html()
		Art.Body, _ = s.Find(".art-body .art-body-inner").Html()
	})

	//Art.Body = strings.Replace(Art.Body, `"`, `\"`, -1)
	//Art.Tags = strings.Replace(Art.Tags, `"`, `\"`, -1)

	return Art
}

// Extract all http** links from a given webpage
func crawl(url string) {
	resp, err := http.Get(url)

	defer func() {
		// Notify that we're done after this function
		wg.Done()
	}()

	if err != nil {
		fmt.Println("ERROR: Failed to crawl \"" + url + "\"")
		return
	}

	b := resp.Body
	defer b.Close()

	z := html.NewTokenizer(b)

	for {
		tt := z.Next()

		switch {
		case tt == html.ErrorToken:
			return
		case tt == html.StartTagToken:
			t := z.Token()

			if t.Data == "a" {
				url := getHref(t)
				if url != "" {

					foundUrls[url] = true
				}
			}
		}
	}

}
