package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sync"
)

const SERVER = "localhost:3006"

var wg sync.WaitGroup

func reportsHandler(w http.ResponseWriter, r *http.Request) {

	// Kick off the crawl process (in parallel)

	go crawl(seedUrl)
	wg.Add(1)

	// Subscribe to both channels

	// We're done!
	wg.Wait()
	for url, _ := range foundUrls {
		art := crawlArticle(url)
		Articles = append(Articles, art)
	}

	test, err := json.Marshal(Articles[0])

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(string(test))

	fmt.Fprintf(w, string(test))
}

func main() {

	fs := http.Dir("../")
	fileHandler := http.FileServer(fs)

	http.Handle("/", fileHandler)
	http.HandleFunc("/reports", reportsHandler)

	log.Printf("Server is running on %s", SERVER)
	err := http.ListenAndServe(SERVER, nil)
	if err != nil {
		log.Fatalln("Server is down.")
	}
}
